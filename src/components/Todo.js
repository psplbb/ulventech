import React from 'react';
import PropTypes from 'prop-types';
import { ListGroupItem, Button } from 'reactstrap';

const Todo = ({ onClick, deleteTodo, completed, text }) => (
  <ListGroupItem
    onClick={onClick}
    style={{
      textDecoration: completed ? 'line-through' : 'none'
    }}
    action
    href="#"
    tag="a"
  >
    {text}
    <Button
      outline
      color="danger"
      size="sm"
      style={{float: 'right'}}
      onClick={deleteTodo}
    >X</Button>
  </ListGroupItem>
)

Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}

export default Todo
