# Tasks Done
- Fixed all identified bugs. (See below for detailed list.)
- Added delete ToDo functionality.
- Added test cases for new delete functionality.

# Bug Fixes

### File: ```src/actions/index.js```

```
-  id: nextTodoId+++,
+  id: nextTodoId++,
```

The increment operator (++) had an additional "+".

```
-export const toggleTodo = () => ({
+export const toggleTodo = id => ({
   type: 'TOGGLE_TODO',
+  id
+})
 })
 ```

 The ```TOGGLE_TODO``` action should include the ```id``` of the ToDo item whose status is being udpated.

 
### File: ```src/components/Footer.js```

```
-    <FilterLink filter={VisibilityFilters.SHOW_COMLETED}>
+    <FilterLink filter={VisibilityFilters.SHOW_COMPLETED}>
```

Fixed typo.

### File: ```src/components/Link.js```

```
+import { Button } from 'reactstrap';
.
.
.

-    <button
+    <Button
        onClick={onClick}
        disabled={active}
        .
        .
        .
     >
       {children}
-    </button>
+    </Button>
```

Used ```Button``` component from ```reactstrap``` instead of the native ```button``` element for a consistent UI.

### File: ```src/components/Todo.js```

```
+    action
+    href="#"
+    tag="a"
   >
     {text}
   </ListGroupItem>
```
Added attributes to ```ListGroupItem``` for improved user experience.


### File: ```src/containers/AddTodo.js```

```
           <InputGroup>
-            <input ref={node => input = node} />
+            <Input innerRef={node => input = node} />
```
Used ```Input``` component from ```reactstrap``` instead of the native ```input``` element for a consistent UI. Updated ```ref``` to ```innerRef```.

### File: ```src/containers/FilterLink.js```

```
-  active: ownProps.filter == state.visibilityFilter
+  active: ownProps.filter === state.visibilityFilter
```
Used strict comparison.

```
-  onClick: dispatch(setVisibilityFilter(ownProps.filter))
+  onClick: () => dispatch(setVisibilityFilter(ownProps.filter))
```
In ```mapDispatchToProps```, the ```onClick``` should be a function.

### File: ```src/reducers/index.js```

```
 export default combineReducers({
-  ...todos,
+  todos,
   visibilityFilter
 })
```
Removed unnecessary usage of spread operator.

### File: ```src/reducers/todos.js```
```
     case 'TOGGLE_TODO':
       return state.map(todo =>
         (todo.id === action.id)
-          ? {completed: !todo.completed}
+          ? {...todo, completed: !todo.completed}
           : todo
       )
```
The ```TOGGLE_TODO``` reducer should update the ```completed``` property only rather than replacing other properties with ```completed```.


# Suggestions

### Functionality
- When creating a ToDo item, save the date and time of creation and when listing the ToDo items, sort in the descenting order of time created. This will ensure that new entries created appears at the top of the list which will improve user experience.

### Coding
- Listing all actions in a single ```actions/index.js``` file will cause the file to grow quicky and become difficult to manage as the application grows. It would be a good practice to split the actions into multiple files.
- Avoid using strings as constants directly in actions and reducers. Instead, create a JavaScript constant and reuse that constant in actions and reducers.
    ```
    // constants.js
    export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'
    ```
    ```
    // REDUCER
    import { VisibilityFilters } from '../actions'
    import { SET_VISIBILITY_FILTER } from '../constants'

    const visibilityFilter = (state = VisibilityFilters.SHOW_ALL, action) => {
    switch (action.type) {
        case SET_VISIBILITY_FILTER:
        return action.filter
        default:
        return state
    }
    }

    export default visibilityFilter
    ```
    ```
    // ACTION
    import { SET_VISIBILITY_FILTER } from '../constants'

    export const setVisibilityFilter = filter => ({
        type: SET_VISIBILITY_FILTER,
        filter
    })
    ```
    This will help avoid name conflicts.
